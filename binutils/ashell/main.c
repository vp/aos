/*
 * binutils: ashell : main.c
 *
 *     ▄▄▄·       .▄▄ ·
 *    ▐█ ▀█ ▪     ▐█ ▀.
 *    ▄█▀▀█  ▄█▀▄ ▄▀▀▀█▄
 *    ▐█ ▪▐▌▐█▌.▐▌▐█▄▪▐█
 *     ▀  ▀  ▀█▄▀▪ ▀▀▀▀
 *
 * Copyright (C) 2009, Sysam, All Rights Reserved
 * Author: Angelo Dureghello <angelo@sysam.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "stdio.h"
#include "string.h"
#include "unistd.h"

#define VERSION		"0.90(alpha)"

#define MAX_PATH	512
#define EXE_PATH	"/system/"

int main ()
{
	char value[2] = {0};
	char exepath[MAX_PATH] = EXE_PATH;
	char *cmd = exepath + (sizeof(EXE_PATH) - 1);
	char *p;

	printf("ashell v." VERSION " started\r\n");

	for(;;) {
prompt:
		p = cmd;
		printf("aos$ ");
		for(;;) {
			value[0] = (unsigned char)getch();
			switch (value[0]) {
			case 0x0a:
			{
				int rval = -1;

				printf("\r");

				if (p == cmd) goto prompt;

				/* terminate the command */
				*p = 0;

				rval = exec(exepath);
				if (rval == -1)
					printf("++ syntax error: "
					"%s command not found\r\n", cmd);

				goto prompt;
			}
			break;
 			default:
				*p++ = value[0];
			break;
			}
		}
	}

	return 0;
}

