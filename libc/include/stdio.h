#ifndef stdio_H_
#define stdio_H_

// TO DO: fopen
typedef struct _io_file { 
	int f;
} 
FILE;

#ifdef __GNUC__
typedef __builtin_va_list va_list;
#define va_arg(list,type) __builtin_va_arg(list,type)
#define va_start(ap,v)  __builtin_va_start(ap,v)
#endif

int putchar(int c);
int printf(const char *format, ...);
int  getch();
FILE *fopen(const char *path, const char *mode);

#endif // stdio_H_
