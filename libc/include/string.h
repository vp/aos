#ifndef string_H_
#define string_H_

typedef unsigned int size_t;

size_t strlen (const char *s);
int strcmp(const char *s1, const char *s2);
int strncmp(const char *s1, const char *s2, size_t n);
char *strchr (const char *haystack, int c);
char *strrchr (const char *haystack, int c);
char *strcpy(char *dest, const char *src);
char *strcat(char *dest, const char *src);
void *memcpy(void *dest, const void *src, size_t n);
void *memset(void *s, int c, size_t n);

#endif //string_H_
