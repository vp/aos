/*
 * libc: string.c
 *
 *     ▄▄▄·       .▄▄ ·
 *    ▐█ ▀█ ▪     ▐█ ▀.
 *    ▄█▀▀█  ▄█▀▄ ▄▀▀▀█▄
 *    ▐█ ▪▐▌▐█▌.▐▌▐█▄▪▐█
 *     ▀  ▀  ▀█▄▀▪ ▀▀▀▀
 *
 * Copyright (C) 2009, Sysam, All Rights Reserved
 * Author: Angelo Dureghello <angelo@sysam.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "string.h"

size_t strlen(const char *s)
{
	int l = 0;

	while (*s++ != 0) l++;

	return l;
}

int strcmp(const char *s1, const char *s2)
{
	int a = strlen(s1);
	int b = strlen(s2);
	int l = 0;

	if (a == 0 || b == 0) return -1;

	while (*s1 != 0) {
		if (*s1 != *s2) return -1;

		l++;
		if (l == a) return 0;

		s1++;
		s2++;
	}

	return -1;
}

int strncmp(const char *s1, const char *s2, size_t n)
{
	int a = strlen(s1);
	int b = strlen(s2);

	if (a < n || b < n) return -1;

	while (n--) {
		if (*s1 != *s2) return -1;

		s1++;
		s2++;
	}

	return 0;
}

char *strchr(const char *h, int c)
{
	while (*h!=0) 
		if (*h++ == c) return (char*)--h;
		
	return (char*)0;
}

char *strrchr(const char *h, int c)
{
	int len = strlen(h);

	char *b = (char*)h+(len-1);

	while (*b != 0)
		if (*b-- == c) return (char*)++b;

	return (char*)0;
}

char *strcpy(char *dest, const char *src)
{
	char *rval = dest;

	while (*src) *dest++ = *src++;

	*dest = 0;

	return rval;
}

char *strcat(char *dest, const char *src)
{
        while (*dest++ != 0);

        strcpy(--dest, src);

	return dest;
}

void *memcpy(void *dest, const void *src, size_t n)
{
	unsigned int s=n%4;
	
	n>>=2;
	while (n--) *(unsigned long*)dest++ = *(unsigned long*)src++;
	while (s--) *(unsigned char*)dest++ = *(unsigned char*)src++;
	
	return dest;
}

void *memset(void *s, int c, size_t n)
{
	while (n--) *(unsigned char*)s = c;

	return s;
}

