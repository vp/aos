#ifndef __vga_h
#define __vga_h

struct _driver_vga {
	int (*console_parse)(const char *);
};

void vga_set_cursor(int x, int y);
void vga_get_cursor(int *x, int *y);
void vga_set_font(const unsigned char *font_to_use);
void vga_write_char(int ch);
void vga_print_string(const char *string);
void vga_select_color(int color);
void vga_draw_xpm_mono(char *xpm[], int x, int y);
void vga_set_cursor_on();
void vga_driver_init(struct _driver_vga *);


#endif // __vga_h