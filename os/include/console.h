#ifndef __console_h
#define __console_h

/*
 * text mode and vga colors
 */
enum {
	CL_BLACK,
	CL_BLUE,
	CL_GREEN,
	CL_CYAN,
	CL_RED,
	CL_MAGENTA,
	CL_BROWN,
	CL_LT_GRAY,
	CL_DK_GRAY,
	CL_LT_BLUE,
	CL_LT_GREEN,
	CL_LT_CYAN,
	CL_LT_RED,
	CL_LT_MAGENTA,
	CL_YELLOW,
	CL_WHITE
};

#define DEFAULT_COLOR CL_GREEN

void console_init();
void console_set_char_color(int c);
void console_unset_char_color();
void console_print(const char *s);
int vt100_parse(const char* vt100_seq);


#endif // __console_h