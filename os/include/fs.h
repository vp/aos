#ifndef __fs_h
#define __fs_h

#include "ktypes.h"

/* total inode limit, fixed */
#define _MAX_INODES		4096
/*
 * fixed for now,
 * idea is that the OS is loaded from an SD or USB stick,
 * so 512 for now is ok
 * but note 4096 sect size HD are in progress
 */
#define _SIZE_SECT		512

/*
 * map before fs
 *
 * what  | offs     | size
 * ------+----------+---------
 * mbr   | 0x0      | 64k
 * vbr   | 0x10000  | 512
 * osbin | 0x10200  | 128k
 * fs    | 0x30200  | variable
 */

/* fixed MBR space in 32 sects for now */
#define _SECT_CNT_MBR		128
/* kernel follows vbr */
#define _SECT_CNT_VBR		1

#define _OFFS_PTABLE		446
/* size of a partition entry */
#define _SIZE_PT_ENTRY		16
/* afs reserved kernel space, 128k  */
#define _SECT_CNT_KERNEL	256
/* superblock offset, after the kernel */
#define _OFFS_SUPERBLOCK	_SECT_CNT_KERNEL + _SECT_CNT_VBR
/* superblock is always of fixed size */
#define _SIZE_SUPERBLOCK	1024
#define _SECT_CNT_SUPERBLOCK	2
/*
 * the block is a logial I/O unit for block devices,
 * and it is a multiple of
 * the sector size
 *
 * afs defaults to 4096
 */
#define _SECT_CNT_BLOCK		8
#define _SIZE_BLOCK		(_SIZE_SECT * _SECT_CNT_BLOCK)

/* directory size */
#define _SIZE_DIR		(sizeof(struct _dentry) * 4096)
#define _INODE_NOT_FOUND	0xffff

/* aos sognature */
#define _SIGN_SUPERBLOCK 	"aos superblock, by Angelo Dureghello (C)"

/*
 * TODO: we can not deliberately write here,
 * we should check that at _BINARY_ENTRY there is nothing about BIOS,
 * getting BIOS used memory intervals (see BIOS int calls).
 *
 * Also, in 64bit, address must be set as page entry. See boot.S
 *
 */
#define _BINARY_ENTRY		0x100000

#define _MAX_PATH		512

typedef uint16_t inode_n;
typedef uint32_t lba;

/* fs specific constrains */
typedef uint32_t _t_date;
typedef uint32_t _t_size;

#pragma pack(0)

/* superblock, contain various information about the partition
 */
struct _superblock
{
	char sign[128];
	lba lba_b_bitmap;
	uint32_t b_bitmap_size;
	lba lba_i_bitmap;
	uint32_t i_bitmap_size;
	lba lba_i_table;
	lba lba_block_0;
	char padding	[360];
};

/* directory:
 * assuming as "directory" a file organized as an array of 16 byte entries
 */
struct _dentry
{
	inode_n _inode;
	char _ename[30];
};

/*
 * inode:
 * contains file informations
 */
struct _s_inode
{
	uint16_t _uid;
	uint16_t _gid;
	uint16_t _rights;
	uint16_t _reserved;
	uint32_t _flink;
	uint32_t _block;
	_t_date _date_c;
	_t_date _date_m;
	_t_date _date_a;
	_t_size _fsize;		// 32  bytes here
	uint64_t _ptrs[12];	// 9   direct, 3 for hierarchy
};

#pragma pack ()

int _fs_read_superblock();
inode_n _fs_get_inode(char *file_name, uint32_t dir_size);
int _fs_read_inode(inode_n n, struct _s_inode *i);
BOOL _fs_load_binary_in_memory(char *file_path, unsigned char *addr);

#endif // __fs_h
