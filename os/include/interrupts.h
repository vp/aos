#ifndef interrupts_H_
#define interrupts_H_

/*
 * pushed by us,
 * after the irq has been received by the isr_XX handler
 */
struct _irq_regs {
	unsigned long r15;
	unsigned long r14;
	unsigned long r13;
	unsigned long r12;
	unsigned long rbp;
	unsigned long rbx;
	unsigned long r11;
	unsigned long r10;
	unsigned long r9;
	unsigned long r8;
	unsigned long rax;
	unsigned long rcx;
	unsigned long rdx;
	unsigned long rsi;
	unsigned long rdi;
	/* pushed by us */
	unsigned long int_no;
	/*
	 * this is prepared,
	 * but can or cannot have sense, depending
	 * if handler pushed it
	 */
	unsigned long err_code;
};

extern void _isr_00	();
extern void _isr_01	();
extern void _isr_02	();
extern void _isr_03	();
extern void _isr_04	();
extern void _isr_05	();
extern void _isr_06	();
extern void _isr_07	();
extern void _isr_08	();
extern void _isr_09	();
extern void _isr_0A	();
extern void _isr_0B	();
extern void _isr_0C	();
extern void _isr_0D	();
extern void _isr_0E	();
extern void _isr_10	();
extern void _isr_11	();
extern void _isr_12	();
extern void _isr_13	();

extern void _isr_20	();	// remapped irqs, IRQ0 to 15
extern void _isr_21	();
extern void _isr_22	();
extern void _isr_23	();
extern void _isr_24	();
extern void _isr_25	();
extern void _isr_26	();
extern void _isr_27	();
extern void _isr_28	();
extern void _isr_29	();
extern void _isr_2A	();
extern void _isr_2B	();
extern void _isr_2C	();
extern void _isr_2D	();
extern void _isr_2E	();
extern void _isr_2F	();

extern void _isr_80	();

void k_load_ivt ();

void _irq_rtc_clock(struct _irq_regs *r);

#endif
