#ifndef __kmsgs_h
#define __kmsgs_h

#include "screen.h"
#include "console.h"

void kmsg_boot(const char *msg);
void kerr(const char *msg);
void kmsg(const char *msg);
void kinfo(const char *msg);
void kdbg(const char *msg);
void kdumphex(char *buff, int size);

/*
 * init operations
 */
void kopok();
void koperr();

#define show_error_code(code) \
	do { \
		kprint("\r\n"); \
		kerr("ERROR CODE "); \
		console_set_char_color(CL_LT_RED); \
		kprintf("%d", code); \
	} while (0);

#define go_into_panic(msg) \
	do{ \
		kcursor_state(false); \
		kprint("\r\n"); \
		kerr(msg); \
		for (;;); \
	} while (0);

#endif // __kmsgs_h
