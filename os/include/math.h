#ifndef __math_H
#define __math_H

int kabs(int n);
int kmin(int v1, int v2);

#endif //  __math_H