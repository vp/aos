#ifndef _screen_h
#define _screen_h

#include "ktypes.h"

void textmode_set_color(int color);
void textmode_print_string(char *string);
void kcursor_state(BOOL state);
void kupdate_cursor();
void kclear_screen();

#endif // _screen_h