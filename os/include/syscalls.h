#ifndef __syscalls_h
#define __syscalls_h

#include "interrupts.h"

int sysc_printf(struct _irq_regs *r);

#endif // _syscalls_H