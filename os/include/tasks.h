#ifndef tasks_h
#define tasks_h

/* task details */
typedef struct task
{
	int id;
	char name[256];
} Task;

/* task linked list */
typedef struct tsklist
{
	struct	tsklist	*next, *prev;
	Task	*tsk;
} TskList;

void task_scheduler_init();
int exec_task(char *name);

#endif // tasks_h

