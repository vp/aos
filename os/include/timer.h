#ifndef __timer_h
#define __timer_h

/*
 * get timestamp from power up
 */

struct _timestamp {
	unsigned long usecs;
	unsigned long secs;
};

void get_timestamp();

void systime_init();
void udelay(int usecs);
void mdelay(int msecs);

#endif // __timer_h
