/*
 * os: isr.S
 *
 *     ▄▄▄·       .▄▄ ·
 *    ▐█ ▀█ ▪     ▐█ ▀.
 *    ▄█▀▀█  ▄█▀▄ ▄▀▀▀█▄
 *    ▐█ ▪▐▌▐█▌.▐▌▐█▄▪▐█
 *     ▀  ▀  ▀█▄▀▪ ▀▀▀▀
 *
 * Copyright (C) 2009, Sysam, All Rights Reserved
 * Author: Angelo Dureghello <angelo@sysam.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


.file "isr.S"

.text
.code64

/*
 * pushal pushes 32 bit registers (pusha 16but)
 * with the order:
 * EAX, ECX, EDX, EBX, original ESP, EBP, ESI, and EDI
 *
 * So we fill here the _irq_regs struct
 */
.macro save
	pushq	%rdi
	pushq	%rsi
	pushq	%rdx
	pushq	%rcx
	pushq	%rax
	pushq	%r8
	pushq	%r9
	pushq	%r10
	pushq	%r11
	pushq	%rbx
	pushq	%rbp
	pushq	%r12
	pushq	%r13
	pushq	%r14
	pushq	%r15
.endm

.macro restore
	popq	%r15
	popq	%r14
	popq	%r13
	popq	%r12
	popq	%rbp
	popq	%rbx
	popq	%r11
	popq	%r10
	popq	%r9
	popq	%r8
	popq	%rax
	popq	%rcx
	popq	%rdx
	popq	%rsi
	popq	%rdi
.endm

.global _isr_00		/* 19 cpu exception isrs */
.global _isr_01
.global _isr_02
.global _isr_03
.global _isr_04
.global _isr_05
.global _isr_06
.global _isr_07
.global _isr_08
.global _isr_09
.global _isr_0A
.global _isr_0B
.global _isr_0C
.global _isr_0D
.global _isr_0E
.global _isr_10
.global _isr_11
.global _isr_12
.global _isr_13

.global _isr_20		/* start of irq remapped isrs, IRQ0 */
.global _isr_21
.global _isr_22
.global _isr_23
.global _isr_24
.global _isr_25
.global _isr_26
.global _isr_27		/* IRQ07, last of master controller */
.global _isr_28		/* IRQ08, slave controller */
.global _isr_29
.global _isr_2A
.global _isr_2B
.global _isr_2C
.global _isr_2D
.global _isr_2E
.global _isr_2F		/* IRQ15, slave controller */

.global _isr_80

/*
 * note here we push a fake error code
 * only for handlers that don't push an
 * error code themself
 */

_isr_00:
	cld
	pushq	$0
	jmp	_isr_common_stub
_isr_01:
	cld
	pushq	$1
	jmp	_isr_common_stub
_isr_02:
	cld
	pushq	$2
	jmp	_isr_common_stub
_isr_03:
	cld
	pushq	$3
	jmp	_isr_common_stub
_isr_04:
	cld
	pushq	$4
	jmp _isr_common_stub
_isr_05:
	cld
	pushq	$5
	jmp	_isr_common_stub
_isr_06:
	cld
	pushq	$6
	jmp	_isr_common_stub
_isr_07:
	cld
	pushq	$7
	jmp	_isr_common_stub
_isr_08:
	cld
	/* error code set as 0 by the exception */
	pushq	$8
	jmp	_isr_common_stub
_isr_09:
	cli
	pushq	$9
	jmp	_isr_common_stub
_isr_0A:
	cld
	/* error code set by the exception */
	pushq	$10
	jmp	_isr_common_stub
_isr_0B:
	cld
	/* error code set by the exception */
	pushq	$11
	jmp	_isr_common_stub
_isr_0C:
	cld
	/* error code set by the exception */
	pushq	$12
	jmp	_isr_common_stub
_isr_0D:
	cld
	/* error code set by the exception */
	pushq	$13
	jmp	_isr_common_stub
_isr_0E:
	cld
	/* error code set by the exception */
	pushq	$14
	jmp	_isr_common_stub
/* vector 15 doesn't exist on amd64 */
_isr_10:
	cld
	pushq	$16
	jmp	_isr_common_stub
_isr_11:
	cld
	pushq	$17
	jmp	_isr_common_stub
_isr_12:
	cld
	pushq	$18
	jmp	_isr_common_stub
_isr_13:
	cld
	pushq	$19
	jmp	_isr_common_stub

	/* first point entered byt int 80h is here */
_isr_80:
	cld
	pushq	$0x80
	jmp	_isr_common_stub

/*
 * in protected mode, what happen at the int 80 call ?
 * Interrupt numbered by immediate byte (0..255).
 * Algorithm:
 * CPU Pushes to stack:
 *	flags register
 *	CS
 *	IP
 * Then sets:
 *      IF = 0
 * Then transfer control to interrupt procedure
 */
_isr_common_stub:
	save
	movq	%rsp, %rdi
	movq	$_isr_handler, %rax
	call	*%rax
	restore
	add	$8, %rsp		/* int no */
	iretq

/* irq requests moved starting from 32 jumps here */

_isr_20:
	cld
	pushq	$32
	jmp	_irq_common_stub
_isr_21:
	cld
	pushq	$33
	jmp	_irq_common_stub
_isr_22:
	cld
	pushq	$34
	jmp	_irq_common_stub
_isr_23:
	cld
	pushq	$35
	jmp	_irq_common_stub
_isr_24:
	cld
	pushq	$36
	jmp	_irq_common_stub
_isr_25:
	cld
	pushq	$37
	jmp	_irq_common_stub
_isr_26:
	cld
	pushq	$38
	jmp	_irq_common_stub
_isr_27:
	cld
	pushq	$39
	jmp	_irq_common_stub
_isr_28:
	cld
	pushq	$40
	jmp	_irq_common_stub
_isr_29:
	cld
	pushq	$41
	jmp	_irq_common_stub
_isr_2A:
	cld
	pushq	$42
	jmp	_irq_common_stub
_isr_2B:
	cld
	pushq	$43
	jmp	_irq_common_stub
_isr_2C:
	cld
	pushq	$44
	jmp	_irq_common_stub
_isr_2D:
	cld
	pushq	$45
	jmp	_irq_common_stub
_isr_2E:
	cli
	pushq	$46
	jmp	_irq_common_stub
_isr_2F:
	cld
	pushq	$47
	jmp	_irq_common_stub

_irq_common_stub:
	save
	movq	%rsp, %rdi		/* push struct _reg * on the stack */
	movq	$_irq_handler, %rax
	call	*%rax
	restore
	add	$8, %rsp		/* int no */
	iretq

.data
retval:	.long		0x00

