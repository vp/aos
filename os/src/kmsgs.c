/*
 * os: kmsgs.c
 *
 *     ▄▄▄·       .▄▄ ·
 *    ▐█ ▀█ ▪     ▐█ ▀.
 *    ▄█▀▀█  ▄█▀▄ ▄▀▀▀█▄
 *    ▐█ ▪▐▌▐█▌.▐▌▐█▄▪▐█
 *     ▀  ▀  ▀█▄▀▪ ▀▀▀▀
 *
 * Copyright (C) 2009, Sysam, All Rights Reserved
 * Author: Angelo Dureghello <angelo@sysam.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "kmsgs.h"
#include "string.h"
#include "console.h"
#include "timer.h"

#include "../../modules/include/vga.h"

#define _M	"*"
#define _I	"i"
#define _E	"e"
#define _P	"!"
#define _D	"-"

#define _O	"["
#define _C	"]"
#define _space	" "

#define _KM	_O _M _C _space
#define _KI	_O _I _C _space
#define _KE	_O _E _C _space "err: "
#define _KD	_O _P _C _space
#define _KP	_O _P _C _space "System Halted! PANIC: "


char tstamp[16];

char *timestamp()
{
	static struct _timestamp ts;

	get_timestamp(&ts);

	tstamp[0] = 0;

	kstrcat(tstamp, zpad(itoa(ts.secs), 5));
	kstrcat(tstamp, ".");
 	kstrcat(tstamp, zpad(itoa(ts.usecs), 6));

	return tstamp;
}

static void print_timestamp()
{
	kprintc("[", CL_BROWN);
	kprintc(timestamp(), CL_BROWN);
	kprintc("] ", CL_BROWN);
}

void kmsg_boot(const char *msg)
{
	print_timestamp();
	kprintc(msg, CL_LT_BLUE);
}

void kmsg(const char *msg)
{
	print_timestamp();
	kprintc(msg, CL_LT_GRAY);
}

void kinfo(const char *msg)
{
	print_timestamp();
	kprintc(msg, CL_LT_GREEN);
}

void kerr(const char *msg)
{
	print_timestamp();
	kprintc(msg, CL_LT_RED);
}

void kdbg(const char *msg)
{
	print_timestamp();
	kprintc(msg, CL_DK_GRAY);
}

#define GRAPHIC_MODE 1

void kopok()
{
#ifdef GRAPHIC_MODE
	int x, y;

	vga_get_cursor(&x, &y);
	vga_set_cursor(73, y);

	kprintc("[ ", CL_YELLOW);
	kprintc("ok", CL_GREEN);
	kprintc(" ]", CL_YELLOW);

	kprint("\r\n");
#endif
}

void koperr()
{
#ifdef GRAPHIC_MODE
	kprint("\r\n");
#endif
}

void kdumphex(char *buff, int size)
{
	int i = 0;

	while (size--) {
		if (!(i % 16)) {
			if (i) kprint("\r\n");
			kprintf("%06X : ", (int)(i << 5));
		}
		kprintf("%02x ", ((int)buff[i] & 0xff) );
		i++;
	}
	kprint("\r\n");
}

