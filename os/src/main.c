/*
 * os: main.c
 *
 *     ▄▄▄·       .▄▄ ·
 *    ▐█ ▀█ ▪     ▐█ ▀.
 *    ▄█▀▀█  ▄█▀▄ ▄▀▀▀█▄
 *    ▐█ ▪▐▌▐█▌.▐▌▐█▄▪▐█
 *     ▀  ▀  ▀█▄▀▪ ▀▀▀▀
 *
 * Copyright (C) 2009, Sysam, All Rights Reserved
 * Author: Angelo Dureghello <angelo@sysam.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "fs.h"
#include "interrupts.h"

#include "kmsgs.h"
#include "kio.h"
#include "string.h"
#include "screen.h"
#include "console.h"
#include "timer.h"
#include "tasks.h"

#include "../../modules/include/vga.h"

void kexec_binary()
{
	__asm__ __volatile__ (
		"	call	*%%rax	\n"
	: :"a"(_BINARY_ENTRY));
}

void init_fs()
{
	struct _s_inode i0;

	kmsg("mounting filesystem, reading fs superblock ...");

	if (! _fs_read_superblock()) {
		koperr();
		go_into_panic("error reading aos superblock\r\n");
	}

	kopok();

	kmsg("checking valid fs root inode ...");

	_fs_read_inode(0, &i0);

	if (i0._uid == 0x01 && i0._gid == 0x01)
		kopok();
	else {
		go_into_panic("\r\nerror bad inode 0\r\n");
	}
}

void kstart_shell()
{
	kmsg("loading a-shell ...");

	/* pre-clear bss area */
	kfmemset((unsigned char*)_BINARY_ENTRY, 0, 0x80000);

	if (_fs_load_binary_in_memory("/system/ashell", 0)) {
		kopok();
		kprint("\r\n");
		vga_set_cursor_on();

		kexec_binary();
	}
}

extern unsigned long code;
extern unsigned long data;
extern unsigned long data_end;
extern unsigned long rodata;
extern unsigned long rodata_end;
extern unsigned long _bss_start;
extern unsigned long _bss_end;

/*
 * NOTE: & on labes are mandatory, we need them offset
 */
void kbssinit()
{
	kmemset((unsigned char *)&_bss_start, 0,
		((unsigned long)&_bss_end) -
		((unsigned long)&_bss_start));
}

#define DBG_KERNEL_BINARY 1

int _kmain()
{
	kbssinit();
	k_load_ivt();
	console_init();

	kmsg_boot("aos monokernel booting ...\r\n");

#ifdef DBG_KERNEL_BINARY
	kprintf("\r\n");
	console_set_char_color(CL_LT_GRAY);
	kprintf("kernel memory configuration:\r\n");
	kprintf("  code .......... 0x%08x\r\n", (&code));
	kprintf("  code size ..... 0x%08x\r\n",
		((unsigned long)&data) - ((unsigned long)&code));
	kprintf("  data .......... 0x%08x\r\n", (&data));
	kprintf("  data size ..... 0x%08x\r\n",
		((unsigned long)&_bss_start) - ((unsigned long)&data));
	kprintf("  rodata ........ 0x%08x\r\n", (&rodata));
	kprintf("  rodata size ... 0x%08x\r\n",
		((unsigned long)&rodata_end) - ((unsigned long)&rodata));
	kprintf("  _bss_start .... 0x%08x\r\n", (&_bss_start));
	kprintf("  _bss_end ...... 0x%08x\r\n", (&_bss_end));
	kprintf("  _bss_size ..... 0x%08x\r\n",
		((unsigned long)&_bss_end) -
		((unsigned long)&_bss_start));
	kprintf("  binary size ....  %8d\r\n",
		((unsigned long)&data_end) - ((unsigned long)&code));
	kprint("\r\n");
#endif
	/*
	 * below some info of services started before video is up
	 */
	kmsg("zeroing bss ...");
	kopok();

	kmsg("initializing interrupts ...");
	kopok();

	kmsg("initializing vga driver ...");
	kopok();

	task_scheduler_init();

	/* init system timer */
	systime_init();

	/* init rootfs */
	init_fs();

	kstart_shell();

	for(;;);

	return 0;
}

extern int _kmain ();
