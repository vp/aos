/*
 * os: string.c
 *
 *     ▄▄▄·       .▄▄ ·
 *    ▐█ ▀█ ▪     ▐█ ▀.
 *    ▄█▀▀█  ▄█▀▄ ▄▀▀▀█▄
 *    ▐█ ▪▐▌▐█▌.▐▌▐█▄▪▐█
 *     ▀  ▀  ▀█▄▀▪ ▀▀▀▀
 *
 * Copyright (C) 2009, Sysam, All Rights Reserved
 * Author: Angelo Dureghello <angelo@sysam.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "string.h"
#include "math.h"
#include "ktypes.h"
#include "kmsgs.h"
#include "console.h"

/*
 * NOTE: why string.h into the kernel ?
 * libc is intended to be used from user space, and it then can uses system
 * calls to interact with the kernel.
 *
 * From kernel, that is a pure binary not linked against any lib, we need
 * kernel binary support routines.
 */

extern unsigned long cur_x;
extern unsigned long cur_y;

/*
 * in 64bit classic #define replacements are not working anymore
 * we use gcc builtins
 */
#ifdef __GNUC__
#define va_arg(list,type) __builtin_va_arg(list,type)
#define va_start(ap,v)  __builtin_va_start(ap,v)
#endif

#define MAX_OPT_STRING  256

static char pbuff[2048];
static char ptemp[MAX_OPT_STRING];

static char * itohex(int val, unsigned char caps)
{
	char *p = ptemp + (MAX_OPT_STRING - 1);
	unsigned char v;
	unsigned int uval = (unsigned int)val;

        *p-- = 0;
        do {
		v = uval % 16;
		if (v > 9)
			v += (caps) ? 55 : 87;
		else
			v += 0x30;

		*p-- = v;
		uval >>= 4;
        } while(uval);

        return ++p;
}

/*
 * read untils space or 0
 */
int atoi(char *s)
{
	int rval = 0;

	while (*s && ((*s >= 0x30) && (*s <= 0x39))) {
		rval *= 10;
		rval += (*s++ - 48);
	}

	return rval;
}

char * itoa(int val)
{
	char *p = ptemp + (MAX_OPT_STRING - 1);
	char sign_m = 0;

	*p-- = 0;

	if (val < 0) sign_m++;

        do {
                *p-- = (kabs(val % 10) + '0'); val /= 10;
        } while(val);

	if (sign_m) *p-- = '-';

        return ++p;
}

/*
 * there must be array allocated backward (previous ptemp usage assumed)
 */
char * zpad(char *sz, int times)
{
        times = times - kstrlen(sz);
        while (times--) {*--sz = '0';}

        return sz;
}

/*
 * there must be array allocated backward (previous ptemp usage assumed)
 */
static char * pad(char *sz, int times)
{
        times = times - kstrlen(sz);

        while (times--) {*--sz = ' ';}

        return sz;
}

static char * get_opts(va_list vl, char **opts)
{
	unsigned char padz = 0;
	unsigned char padc = 0;
	unsigned char padlj = 0;
	unsigned char opt;

	char *rval;

	for (;;) {
		opt = **opts;

		switch(opt) {
		case '0':
			padz++;
		break;
		case '-':
			/* still no pad set ? */
			if (padc == 0) {
				/* so it ask for left justify */
				padlj = 1;
			}
		break;
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			padc *= 10;
			padc += (opt - 0x30);
		break;
		case 'X':
			rval = itohex(va_arg(vl, int), 1);
		break;
		case 'x':
			rval = itohex(va_arg(vl, int), 0);
		break;
		case 'd':
			rval = itoa(va_arg(vl, int));
		break;
		case 's':
			rval = va_arg(vl, char*);
		break;
		default:
			// end of possible specifiers, apply format
			if (padc) {
				if (!padlj) {
					rval = (padz) ? zpad(rval, padc) :
						pad(rval, padc);
				} else {
					int c, i, n, q;

					q = kstrlen(rval);
					n = padc - q;

					kstrcpy(ptemp, rval);

					for (c = 0, i = q; c < n; ++c, ++i)
						ptemp[i] = ' ';

					ptemp[i] = 0;

					rval = ptemp;
				}
			}
			// end exit
			return rval;
		}
		(*opts)++;
	}
	return 0;
}

/*
 * NOTE: on 64 bit passing a reference to va_list is no more
 * accepted
 */
int kprintf_s(char* s, va_list vl)
{
	char *bi = s; /* next block init */
	char *ps = s;
	char *pp = pbuff;
	char *conv;

	while (*ps) {
		if (*ps == '%') {
			kstrncpy(pp, bi, ps - bi);
			pp += (ps - bi);
			++ps;
			if ((conv = get_opts(vl, &ps))) {
				kstrcpy(pp, conv);
				pp += kstrlen(conv);
				bi = ps;
			}
			else goto exit;
		}
		ps++;
	}
	/* terminated */
	kstrcpy(pp, bi);
	console_print((char*)pbuff);
exit:
	return kstrlen(pbuff);
}

int kprintf(char* s, ...)
{
	va_list vl;
	va_start(vl, s);

	return kprintf_s(s, vl);
}

void kstrcat(char *dst, char *src)
{
        while (*dst++ != 0);

        kstrcpy(--dst, src);
}

void kstrcpy(char *dst, char *src)
{
        while (*src != 0) *dst++ = *src++;

        *dst = 0;
}

void kstrncpy(char *dst, char *src, int len)
{
        while (len--) *dst++ = *src++;

        *dst = 0;
}

int kstrlen(char *s)
{
	int len = 0;

	while (*s++) len++;

	return len;
}

int kstrcmp(char *s1, char *s2)
{
	int rval;

	__asm__ __volatile__ (
		"	xor	%%ecx, %%ecx		\n"
		"	xor	%%dx, %%dx		\n"
		"1:	mov	%%ds:(%%esi), %%eax	\n"
		"	mov	%%ds:(%%edi), %%ebx	\n"
		"	or	$0x04, %%dl		\n"
		"2:	or	%%al, %%al		\n"
		"	jz	3f			\n"
		"	or	%%bl, %%bl		\n"
		"	jz	3f			\n"
		"	xor	%%al, %%bl		\n"
		"	jnz	4f			\n"
		"	mov	$1, %%dh		\n"
		"	shr	$8, %%eax		\n"
		"	shr	$8, %%ebx		\n"
		"	dec	%%dl			\n"
		"	jnz	2b			\n"
		"	add	$4, %%esi		\n"
		"	add	$4, %%edi		\n"
		"	jmp	1b			\n"
		"3:	or	%%dh, %%dh		\n"
		"	jnz	5f			\n"
		"4:	movl	$0xffffffff, %%ecx	\n"
		"5:					\n"
		: "=c"(rval)
		: "S"(s1), "D"(s2)
		);

	return rval;
}

char * kstrchr(char *src, int val)
{
	char *pos;

	__asm__ __volatile__ (
		"	xor	%%edi, %%edi		\n"
		"1:	mov	%%ds:(%%esi), %%al	\n"
		"	or	%%al, %%al		\n"
		"	jz	3f			\n"
		"	cmp	%%bl, %%al		\n"
		"	je	2f			\n"
		"	inc	%%esi			\n"
		"	jmp	1b			\n"
		"2:	mov	%%esi, %%edi		\n"
		"3:					\n"
		: "=D"(pos)
		: "S"(src), "b"(val)
	);

	return pos;
}

void kmemset(unsigned char *dst, int val, size_t count)
{
	while (count--) *dst++ = val;
}

/*
 * fast memset, but still not fastest
 */
void kfmemset(unsigned char *dst, int val, size_t count)
{
	__asm__ __volatile__ (
		"shr	$6, %%ecx		\n"
		"mov	$8, %%r8		\n"
		"loopp:				\n"
		"shl	$8, %%rdx		\n"
		"mov	%%dh, %%dl		\n"
		"dec	%%r8			\n"
		"test	%%r8, %%r8		\n"
		"jnz	loopp			\n"
		"loopm:				\n"
		"mov	%%rdx, 0(%%rdi)		\n"
		"mov	%%rdx, 8(%%rdi)		\n"
		"mov	%%rdx, 16(%%rdi)	\n"
		"mov	%%rdx, 24(%%rdi)	\n"
		"mov	%%rdx, 32(%%rdi)	\n"
		"mov	%%rdx, 40(%%rdi)	\n"
		"mov	%%rdx, 48(%%rdi)	\n"
		"mov	%%rdx, 56(%%rdi)	\n"
		"add	$64, %%rdi		\n"
		"dec	%%ecx			\n"
		"test	%%ecx, %%ecx		\n"
		"jnz	loopm			\n"
	: : "D"(dst), "c"(count), "d"(val) : "r8");
}

/* TO DO in quadword */
void kmemcpy(unsigned char *dst, unsigned char *src, size_t count)
{
	while (count--) *dst++ = *src++;
}

void kfmemcpy(unsigned char *dst, unsigned char *src, size_t count)
{
	__asm__ __volatile__ (
		"shr	$6, %%ecx		\n"
		"loopq:				\n"
		"movq	0(%%esi), %%r8		\n"
		"movq	8(%%esi), %%r9		\n"
		"movq	16(%%esi), %%r10	\n"
		"movq	24(%%esi), %%r11	\n"
		"movq	32(%%esi), %%r12	\n"
		"movq	40(%%esi), %%r13	\n"
		"movq	48(%%esi), %%r14	\n"
		"movq	56(%%esi), %%r15	\n"
		"movq	%%r8, 0(%%edi)		\n"
		"movq	%%r9, 8(%%edi)		\n"
		"movq	%%r10, 16(%%edi)	\n"
		"movq	%%r11, 24(%%edi)	\n"
		"movq	%%r12, 32(%%edi)	\n"
		"movq	%%r13, 40(%%edi)	\n"
		"movq	%%r14, 48(%%edi)	\n"
		"movq	%%r15, 56(%%edi)	\n"
		"add	$64, %%esi		\n"
		"add	$64, %%edi		\n"
		"dec	%%ecx			\n"
		"test	%%ecx, %%ecx		\n"
		"jnz	loopq			\n"
	: :"S"(src), "D"(dst), "c"(count) :);

}

/*
 * TODO: see why it is not working
 *
 * gives canonical rip violation
 */
void kxmemcpy(unsigned char *dst, unsigned char *src, size_t count)
{
	__asm__ __volatile__ (
		"shr		$7, %%rcx		\n"
		"loopc:					\n"
		"prefetchnta	128(%%rsi)		\n"
		"prefetchnta	160(%%rsi)		\n"
		"prefetchnta	192(%%rsi)		\n"
		"prefetchnta	224(%%rsi)		\n"
		"movdqa		0(%%rsi), %%xmm0	\n"
		"movdqa		16(%%rsi), %%xmm1	\n"
		"movdqa		32(%%rsi), %%xmm2	\n"
		"movdqa		48(%%rsi), %%xmm3	\n"
		"movdqa		64(%%rsi), %%xmm4	\n"
		"movdqa		80(%%rsi), %%xmm5	\n"
		"movdqa		96(%%rsi), %%xmm6	\n"
		"movdqa		112(%%rsi), %%xmm7	\n"
		"movntdq	%%xmm0, 0(%%rdi)	\n"
		"movntdq	%%xmm1, 16(%%rdi)	\n"
		"movntdq	%%xmm2, 32(%%rdi)	\n"
		"movntdq	%%xmm3, 48(%%rdi)	\n"
		"movntdq	%%xmm4, 64(%%rdi)	\n"
		"movntdq	%%xmm5, 80(%%rdi)	\n"
		"movntdq	%%xmm6, 96(%%rdi)	\n"
		"movntdq	%%xmm7, 112(%%rdi)	\n"
		"add		$128, %%rsi		\n"
		"add		$128, %%rdi		\n"
		"dec		%%rcx			\n"
		"test		%%rcx, %%rcx		\n"
		"jnz		loopc			\n"
	: :"S"(src), "D"(dst), "c"(count) :);
}

void kprint(const char *string)
{
	console_print(string);
}

void kprintc(const char *string, int c)
{
	console_set_char_color(c);
	console_print(string);
	console_unset_char_color();
}

/*
 * we should not use kernel printf but single char putch and
 * implement printf in libc.
 *
 * Actually, it's easier to use kprinf to avoid double printf implementation.
 */
long syscall_printf(char* s, va_list vl)
{
	return kprintf_s(s, vl);
}

long syscall_puts(const char *s)
{
	kprint(s);

	return 0;
}
