/*
 * os: tasks.c
 *
 *     ▄▄▄·       .▄▄ ·
 *    ▐█ ▀█ ▪     ▐█ ▀.
 *    ▄█▀▀█  ▄█▀▄ ▄▀▀▀█▄
 *    ▐█ ▪▐▌▐█▌.▐▌▐█▄▪▐█
 *     ▀  ▀  ▀█▄▀▪ ▀▀▀▀
 *
 * Copyright (C) 2009, Sysam, All Rights Reserved
 * Author: Angelo Dureghello <angelo@sysam.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "tasks.h"
#include "string.h"
#include "fs.h"

#define LOAD_ADDR	0x180000

/*
 * calling convention, regs used esi,edx .. etc
 */
static void exec_binary(int argc, char**argv)
{
	__asm__ __volatile__ (
		"	call	*%%rax	\n"
	: :"S"(argv), "D"(argc), "a"(LOAD_ADDR));
}

/* TO DO: CHECK to stay in 512K now */

void task_scheduler_init()
{
	/*
	 * zero here some bss space for loaded proc, but note,
	 * this is just a temporary hack
	 */
	kfmemset((unsigned char*)0x180000, 0, (0x80000 - 1));
}

#define MAX_ARGS   16

int argc;
char *argv[MAX_ARGS];

static void parse_params(char *cmd)
{
	char *c = cmd;

	argc = 1;
	while ((c = kstrchr(c, ' '))) {
		if (*++c) {
			argv[argc] = c;
			argc++;
		}
	}
}

/*
 * TODO: to study simple multitasking
 * for now, monobinary
 */
int exec_task(char *file_name)
{
	int retval = 0;

	parse_params(file_name);

	/* file name in rdi */
	if (_fs_load_binary_in_memory(file_name, (unsigned char*)LOAD_ADDR)) {
		exec_binary(argc, argv);
		retval = 1;
	}
	return retval;
}

long syscall_exec(char* binary)
{
	long rval;
	static char file_path[_MAX_PATH];

	/*
	 * pointers can be const *, so we have no rights to
	 * modify the pointed content
	 */
	kstrcpy(file_path, binary);

	rval = 0;

	if (!exec_task(file_path))
		rval = -1;

	return rval;
}
