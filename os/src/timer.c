/*
 * os: timer.c
 *
 *     ▄▄▄·       .▄▄ ·
 *    ▐█ ▀█ ▪     ▐█ ▀.
 *    ▄█▀▀█  ▄█▀▄ ▄▀▀▀█▄
 *    ▐█ ▪▐▌▐█▌.▐▌▐█▄▪▐█
 *     ▀  ▀  ▀█▄▀▪ ▀▀▀▀
 *
 * Copyright (C) 2009, Sysam, All Rights Reserved
 * Author: Angelo Dureghello <angelo@sysam.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "kmsgs.h"
#include "kio.h"
#include "timer.h"
#include "interrupts.h"

static void init_rtc()
{
	__asm__ __volatile__ (
		"	cli			\n"
		"	outp	$0x70, $0x8b	\n"
		"	inp	$0x71		\n"
		"	mov	%al, %bl	\n"
		"	outp	$0x70, $0x8b	\n"
		"	or	$0x40, %bl	\n"
		"	outp	$0x71, %bl	\n"
		/*
		 * set fastest rate now for 122us (3)
		 */
		"	outp	$0x70, 0x8a	\n"
		"	inp	$0x71		\n"
		"	mov	%al, %bl	\n"
		"	outp	$0x70, 0x8a	\n"
		"	and	$0xf0, %bl	\n"
		"	or	$0x03, %bl	\n"
		"	outp	$0x71, %bl	\n"
		"	sti			\n"
		);
}

static unsigned long systicks = 0;

/*
 * system clock, from RTC, trig every 122us
 */
void _irq_rtc_clock(struct _irq_regs *r)
{
	__asm__ __volatile__ (
		"	outp	$0x70, $0x0C	\n"
		"	inp	$0x71		\n"
	);

	systicks++;
}

void get_timestamp(struct _timestamp *ts)
{
	/*
	 * systicks are 122us
	 */
	unsigned long us = systicks * 122;

	ts->usecs = us;
	ts->secs  = ts->usecs / 1000000;
}

void systime_init()
{
	kmsg("initializing system timer ...");

	init_rtc();

	kopok();
}

/** to do with pit **/

void udelay(int usecs)
{
	while (usecs--) {
		__asm__ __volatile__ (
		"	mov	$0x100,%%ecx	\n"
		"1:	dec	%%ecx		\n"
		"	or	%%ecx,%%ecx	\n"
		"	jnz	1b		\n"
		::);
	}
}

void mdelay(int msecs)
{
	udelay(msecs*1000);
}
