/*
 * mkafs : a tool to create the a(angelo)fs filesystem
 *
 *     ▄▄▄·       .▄▄ ·
 *    ▐█ ▀█ ▪     ▐█ ▀.
 *    ▄█▀▀█  ▄█▀▄ ▄▀▀▀█▄
 *    ▐█ ▪▐▌▐█▌.▐▌▐█▄▪▐█
 *     ▀  ▀  ▀█▄▀▪ ▀▀▀▀
 *
 * Copyright (C) 2009, Sysam, All Rights Reserved
 * Author: Angelo Dureghello <angelo@sysam.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "../../os/include/fs.h"

/*
 * map:
 *
 *      desc                 offs      size
 * -----------------------------------------------------------------------
 *	mbr                  0         512 Bytes
 *	vbr kernel           0x10000   (64 * 512 sectors)
 *
 *      -- here start the FS (@ 63 + 64 kernel sects)
 *
 *	superblock           0x18000   1024
 *	block bitmap         0x18400   min 1 block
 *	inode bitmap         ?         min 1 block
 *	inode table          ?         min 1 block
 *	block 0              ?
 */

#define lba_to_bytes(x) (x * _SIZE_SECT)

void write_superblock(int f, unsigned char *ptable, struct _superblock *s)
{
	lba lba_start, lba_sect_cnt, lba_superblock;
	size_t sect_count, blocks;

	lba_start = *(uint32_t *)&ptable[8];
	lba_sect_cnt = *(uint32_t *)&ptable[12];

	lba_superblock = lba_start + (_SECT_CNT_VBR + _SECT_CNT_KERNEL);

	// LBA = (C·HeadsPerCyl + H)·SecPerTrk + S-1
	// for cf seen as ide, use 32 heads, 63 sector/track XXX cylinders
	printf("* starting active partition lba = %u, sectors = %u\r\n",
		lba_start, lba_sect_cnt);
	printf("* generating superblock\r\n");

	memset(s, 0, sizeof(struct _superblock));
	memcpy(s->sign, _SIGN_SUPERBLOCK, sizeof(_SIGN_SUPERBLOCK) - 1);

	// free blocks bitmap location
	s->lba_b_bitmap = lba_superblock + _SECT_CNT_SUPERBLOCK;
	s->b_bitmap_size = (lba_sect_cnt / _SECT_CNT_BLOCK) / 8;
	if ((lba_sect_cnt / _SECT_CNT_BLOCK) % 8) s->b_bitmap_size++;

	// get b_bitmap size in sects
	blocks = s->b_bitmap_size / _SIZE_BLOCK;
	if (s->b_bitmap_size % _SIZE_BLOCK) blocks++;

	s->lba_i_bitmap = s->lba_b_bitmap + (blocks * _SECT_CNT_BLOCK);
	s->i_bitmap_size = _MAX_INODES / 8;
	if (_MAX_INODES % 8) s->i_bitmap_size++;

	// get i_bitmap size in sects
	blocks = s->i_bitmap_size / _SIZE_BLOCK;
	if (s->i_bitmap_size % _SIZE_BLOCK) blocks++;
	s->lba_i_table  = s->lba_i_bitmap + (blocks * _SECT_CNT_BLOCK);

	blocks = (_MAX_INODES * sizeof(struct _s_inode)) / _SIZE_BLOCK;
	if ((_MAX_INODES * sizeof(struct _s_inode)) % _SIZE_BLOCK) blocks++;

	// first useful block location
	s->lba_block_0 = s->lba_i_table + (blocks * _SECT_CNT_BLOCK);

	// aos superblock, are 128 bytes

	lseek(f, lba_to_bytes(lba_superblock), SEEK_SET);
	size_t written = write(f, (unsigned char*)s, sizeof(struct _superblock));
	if (written != sizeof(struct _superblock)) {
		printf ("++ ERROR writing superblock\n");
		exit (1);
	}

	printf("  sb signature     : %s\r\n", s->sign);
	printf("  lba superblock   : %u\r\n", lba_superblock);
	printf("  lba block bitmap : %u\r\n", s->lba_b_bitmap);
	printf("  lba inode bitmap : %u\r\n", s->lba_i_bitmap);
	printf("  lba inode table  : %u\r\n", s->lba_i_table);
	printf("  lba block 0      : %u\r\n", s->lba_block_0);
}

void write_bitmap(int f, lba lba_bitmap, size_t size)
{
	int x;

	lseek(f, lba_to_bytes(lba_bitmap), SEEK_SET);
	// free bitmap is all to 0
	for (x=0; x<size; x++) write(f, "\x00", 1);
}

int main(int argc, char *argv[])
{
	int f;
	
	if (argc != 2) {
		printf("mkafs - tool to create aos filesystem\r\n");
		printf("usage : mkafs /dev/sdb\r\n");
		
		return EXIT_FAILURE;
	} 
	if ((f = open(argv[1], O_RDWR)) != -1) {
		unsigned char mbr[512];

		printf("* reading mbr\r\n");

		lseek(f, 0, SEEK_SET);
		
		if ((read(f, mbr, 512)) == 512) {
			int ptable = 446;

			printf ("* mbr read successfully\r\n");

			while (mbr[ptable] != 0x80) ptable+=16;
			
			if (ptable <= 494) {
				struct	_superblock s;

				printf("* active partition found\r\n");
				write_superblock(f, &mbr[ptable], &s);
				
				printf("* generating free inodes bitmap\r\n");
				write_bitmap(f, s.lba_b_bitmap, s.b_bitmap_size);
				
				printf("* generating free blocks bitmap\r\n");
				write_bitmap(f, s.lba_i_bitmap, s.i_bitmap_size);
				
				printf("* process terminated successfully\r\n");
				
				close(f);
			} else {
				printf("* no active partition found\r\n");
				return EXIT_FAILURE;
			}
		}
	} else {
		printf("* cant open device\r\n");
	}

	return EXIT_SUCCESS;
}
