/**
 * command.cc
 *
 * command prompt tools
 *
 **/
 
#include "command.hh"

#include <iostream>
#include <stdio.h>
#include <termios.h>
#include <string.h>
#include <stdlib.h>

#include <ncurses.h>

using namespace std;

int cmd::igetch()
{
	static struct termios term, back;
	int ret=-1;

	tcgetattr (0, &term);
	memcpy (&back, &term, sizeof(term));

	term.c_lflag &= ~(ICANON|ECHO);
	term.c_cc[VTIME] = 0;
	term.c_cc[VMIN] = 1;

	tcsetattr(0, TCSANOW, &term);
	ret = getchar();
	tcsetattr(0, TCSANOW, &back);

	return ret;
}

void cmd::console_mode_start()
{
	system("clear");
}

void cmd::console_mode_stop()
{
	cout << "\n\npress a key to continue ...\n";
	igetch();
	system("clear");
}

void cmd::console_command(const std::string &command)
{
	system 	(command.c_str());
	fflush	(stdout);
}

